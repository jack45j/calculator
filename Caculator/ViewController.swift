//
//  ViewController.swift
//  Caculator
//
//  Created by 林翌埕 on 2017/12/6.
//  Copyright © 2017年 YochaStudio. All rights reserved.
//

import UIKit
import Foundation

extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy:1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

class ViewController: UIViewController {
    @IBOutlet weak var uiLabelBottomPannel: UILabel!
    @IBOutlet weak var uiLabelTopPannel: UILabel!
    @IBOutlet weak var uiLabelArithmeticSymbol: UILabel!
    @IBAction func pressedNumberButton(_ sender: UIButton) {
        if((uiLabelBottomPannel.text?.count)! < 20) {
            let BottomPannelNumber = uiLabelBottomPannel.text
            uiLabelBottomPannel.text = ""
            uiLabelBottomPannel.text = "\(BottomPannelNumber ?? "")\(sender.titleLabel?.text ?? "")"
        }
    }
    @IBAction func pressedAllClearButton(_ sender: Any) {
        cleanAlluiLabel()
    }
    @IBAction func pressedMinusPlusSwitchButton(_ sender: Any) {
        uiLabelBottomPannel.text = "\((Float(uiLabelBottomPannel.text!)! * -1).clean)"
        Double(uiLabelBottomPannel.text!)! < 0.0 ? print("Switch positive number to negative.") : print("Switch negative number to positive.")
    }
    @IBAction func pressedArithmeticButtons(_ sender: UIButton) {
        if(uiLabelTopPannel.text?.isEmpty)! {
            print("uiLabelBottomPannels number \(uiLabelBottomPannel.text!) as first number.")
            uiLabelArithmeticSymbol.text = "\(sender.titleLabel?.text ?? "")"
            uiLabelTopPannel.text = uiLabelBottomPannel.text
            uiLabelBottomPannel.text = ""
        }
    }
    @IBAction func pressedPercentButton(_ sender: Any) {
        if(!(uiLabelBottomPannel.text?.isEmpty)!) {
            if(uiLabelArithmeticSymbol.text?.isEmpty)! {
                uiLabelBottomPannel.text = "\(Float(uiLabelBottomPannel.text!)! / 100)"
            }
        } else {
            
        }
    }
    @IBAction func pressedResultButton(_ sender: Any) {
        if(!(uiLabelTopPannel.text?.isEmpty)!) {
            let FirstNumber = Float(uiLabelTopPannel.text!)
            let SecondNumber = Float(uiLabelBottomPannel.text!)
            print("Set \(uiLabelArithmeticSymbol.text!) as arithmetic symbol.")
            print("Calculate \(uiLabelTopPannel.text!) \(uiLabelArithmeticSymbol.text!) \(uiLabelBottomPannel.text!)")
            print("uiLabelBottomPannels number \(uiLabelBottomPannel.text!) as second number.")
            calculate(FirstNumber!, SecondNumber!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func calculate(_ FirstNumber:Float,_ SecondNumber:Float) {
        switch uiLabelArithmeticSymbol.text! {
        case "+":
            setAnswer(FirstNumber + SecondNumber)
            break
        case "-":
            setAnswer(FirstNumber - SecondNumber)
            break
        case "x":
            setAnswer(FirstNumber * SecondNumber)
            break
        case "/":
            setAnswer(FirstNumber / SecondNumber)
            break
        default:
            break
        }
    }
    func setAnswer(_ Answer:Float) {
        uiLabelBottomPannel.text = "\(Answer.clean)"
        print("Set answer result as \(Answer.clean)")
        uiLabelTopPannel.text = ""
        uiLabelArithmeticSymbol.text = ""
    }
    func cleanAlluiLabel() {
        uiLabelBottomPannel.text = ""
        uiLabelTopPannel.text = ""
        uiLabelArithmeticSymbol.text = ""
        print("All Cleared.")
    }
}

